# Database Dashboard

# Prerequisite
  - Drupal 9 - 10
  - mysql

## Installation
Download this module with composer and install it with drush or through the UI.

## Configuration
Add to your settings.php or settings.local.php the following lines:
```
$databases['schema']['default'] = [
  'host' => 'mariadb', // To Adapt
  'database' => 'information_schema',
  'username' => 'drupal', // To Adapt
  'password' => 'drupal', // To Adapt
  'prefix' => '',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];
```
## Usage

Head to /admin/reports/database and relax.
