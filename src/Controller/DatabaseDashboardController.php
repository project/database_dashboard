<?php

namespace Drupal\database_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Mysql Dashboard routes.
 */
class DatabaseDashboardController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('database'));
  }

  /**
   * Builds the response.
   */
  public function dashboard() {
    $databaseName = $this->connection->getConnectionOptions()['database'];
    $data = [];
    $con = Database::getConnection('default', 'schema');
    $res = $con->query('SELECT table_schema AS "Database name", SUM(data_length + index_length) / 1024 / 1024 / 1024 AS "Size (GB)" FROM TABLES where engine=\'InnoDB\' GROUP BY table_schema;');
    $data['database_size'] = $res->fetchAllKeyed();

    $res = $con->query("SELECT table_name AS `table`, round(((data_length + index_length) / 1024 / 1024), 2) `size` FROM information_schema.TABLES WHERE table_schema = '$databaseName' ORDER BY (data_length + index_length) DESC LIMIT 20;");
    $data['tables_sizes'] = $res->fetchAllKeyed();

    $res = $con->query("SELECT TABLE_NAME, TABLE_ROWS FROM `information_schema`.`tables` WHERE `table_schema` = '$databaseName' order by TABLE_ROWS DESC LIMIT 20;");
    $data['tables_rows'] = $res->fetchAllKeyed();

    $res = $con->query("SELECT table_name AS `table`, round(((data_length + index_length) / 1024 / 1024), 2) `size` FROM information_schema.TABLES WHERE table_schema = '$databaseName' AND TABLE_NAME like 'cache%' ORDER BY (data_length + index_length) DESC LIMIT 20;");
    $data['tables_cache_sizes'] = $res->fetchAllKeyed();

    $res = $con->query("SELECT TABLE_NAME, TABLE_ROWS FROM `information_schema`.`tables` WHERE `table_schema` = '$databaseName' AND TABLE_NAME like 'cache%' order by TABLE_ROWS DESC LIMIT 20;");
    $data['tables_cache_rows'] = $res->fetchAllKeyed();

    $build['content'] = [
      '#theme' => 'database_dashboard',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'database_dashboard/dashboard',
        ],
      ],
      '#cache' => ['max-age' => 0],
    ];

    return $build;
  }

}
